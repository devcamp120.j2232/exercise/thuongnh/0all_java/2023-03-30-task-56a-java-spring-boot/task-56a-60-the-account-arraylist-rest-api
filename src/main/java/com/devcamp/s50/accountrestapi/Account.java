package com.devcamp.s50.accountrestapi;

public class Account {
    private String id;
    private String name;
    private  int balance;   // số dư   

    // khởi tạo với 2 phương thức 
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    // khởi tạo với 3 phương thức 
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }
    //  thêm số tiền vào số dư
    public void credit(int amount) {
        this.balance = amount + balance;
    }

    // rút tiền từ số dư 
    public void debit(int amount) {
        this.balance = amount + balance;

        if(amount <= balance){
            // trừ đi số dư khoảng amount 
            this.balance = balance - amount;
        }
        else {
            System.out.println("Amount exceeds balance return balance");
        }
    }
    
}

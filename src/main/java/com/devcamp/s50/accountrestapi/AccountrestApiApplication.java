package com.devcamp.s50.accountrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountrestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountrestApiApplication.class, args);
	}

}

package com.devcamp.s50.accountrestapi;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class getApiAccount {
    @GetMapping("/accounts")
    public ArrayList<Account> getEmployees(){

        //  khởi tạo 3 đối tượng sinh viên 

        Account taikhoan1 = new Account( "thuong", "nguyen", 5000);
        Account taikhoan2 = new Account( "huy", "dao", 6000);
        Account taikhoan3 = new Account( "vinh", "le", 5000);
        // in thông tin của ba nhân viên
        System.out.println(taikhoan1);
        System.out.println(taikhoan2);
        System.out.println(taikhoan3);
        // thêm ba nhân viên vào 1 danh sách

        ArrayList<Account> arrtaikhoan = new ArrayList<Account>();
        arrtaikhoan.add(taikhoan1);
        arrtaikhoan.add(taikhoan2);
        arrtaikhoan.add(taikhoan3);
        return arrtaikhoan;
    }
    
}
